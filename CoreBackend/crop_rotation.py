import pandas as pd
import numpy as np
from pandasql import *
from pprint import pprint
import operator
import datetime

current_crop_info_df=""
current_nutrients_df=""
farmer_info_df = ""
favourable_fertilizer_df = ""
fertilizer_content_df = ""

def query_execute(q):
	return sqldf(q,globals())

def load_data():
	file_name1	= "./Data/current_crop_info.csv"
	file_name2 = "./Data/current_nutrients.csv"
	file_name3 = "./Data/farmer_info.csv"
	file_name4 = "./Data/favourable_fertilizer.csv"
	file_name5 = "./Data/fertilizer_content.csv"
	
	global current_crop_info_df
	global current_nutrients_df
	global farmer_info_df
	global favourable_fertilizer_df
	global fertilizer_content_df
	
	current_crop_info_df = pd.read_csv(file_name1)
	current_nutrients_df = pd.read_csv(file_name2)
	farmer_info_df = pd.read_csv(file_name3)
	favourable_fertilizer_df = pd.read_csv(file_name4)
	fertilizer_content_df = pd.read_csv(file_name5)
	
	#return current_crop_info_df,current_nutrients_df,farmer_info_df,favourable_fertilizer_df,fertilizer_content_df
	
	
def get_current_crop_info(farmer_id):
	query = "select * from current_crop_info_df where Farmer_Id ="+str(farmer_id)
	return query_execute(query).tail(n=1).reset_index(drop=True)
	
	
def get_current_nutrients_info(farmer_id):
	query = "select * from current_nutrients_df where Farmer_Id ="+str(farmer_id)
	return query_execute(query).tail(n=1).reset_index(drop=True)

def get_required_nutrient(data_frame):
	values = [data_frame['K'][0],data_frame['N'][0],data_frame['P'][0],data_frame['S'][0]]
	index,value = min(enumerate(values),key = operator.itemgetter(1))
	name = ['K','N','P','S']
	return name[index],value

def get_surplus_nutrient(data_frame):
	values = [data_frame['K'][0],data_frame['N'][0],data_frame['P'][0],data_frame['S'][0]]
	index,value = max(enumerate(values),key = operator.itemgetter(1))
	name = ['K','N','P','S']
	return name[index],value
	
def get_fertilizer_with_max_content(content_name):
	query = "select Fertilizer_Name from fertilizer_content_df order by "+content_name+" desc LIMIT 1";
	return query_execute(query)['Fertilizer_Name'][0]

	
def get_favourable_crops(fertilizer_name):
	query = "select Crop_Name from favourable_fertilizer_df where Fertilizer='"+fertilizer_name+"'"
	return list(query_execute(query)['Crop_Name'])


	
	
def check_farmer_exists(farmer_id):
	load_data()	
	query = "SELECT count(*) as count FROM farmer_info_df WHERE Farmer_Id="+str(farmer_id)
	result = query_execute(query)
	#print(result['count'][0])
	if(result['count'][0] ==0):
		return False
	else:
		return True
		

#farmer_id,crop name ,area,K,N,P,S


def insert_crop_info(farmer_id,crop_name,area,K,N,P,S):
	load_data()	
	global current_crop_info_df
	global current_nutrients_df
	current_date = datetime.datetime.now()
	my_date_format = str(current_date.day)+'-'+str(current_date.month)+'-'+str(current_date.year)
	
	
	insert_record = [[my_date_format,farmer_id,crop_name,area]]
	df_new = pd.DataFrame(insert_record,columns = ['Date','Farmer_Id','Crop_Name','Area'])
	with open('./Data/current_crop_info.csv', 'a') as f:
		df_new.to_csv(f,header=False,index=False)
	current_crop_info_df = pd.read_csv('./Data/current_crop_info.csv')
	
	
	insert_record = [[my_date_format,farmer_id,K,N,P,S]]
	df_new = pd.DataFrame(insert_record,columns = ['Date','Farmer_Id','K','N','P','S'])
	with open('./Data/current_nutrients.csv', 'a') as f:
		df_new.to_csv(f,header=False,index=False)
	current_nutrients_df = pd.read_csv('./Data/current_nutrients.csv')
	
	
def crop_rotation(farmer_id):

        load_data()
        #farmer_id = 1111
        print(farmer_id)
        ci_df = get_current_crop_info(farmer_id)
        cn_df = get_current_nutrients_info(farmer_id)

        surplus_nutrient,surplus_value = get_surplus_nutrient(cn_df)
        deficient_nutrient,deficient_value = get_required_nutrient(cn_df)
        fert1 = get_fertilizer_with_max_content(deficient_nutrient)

        fert2 = get_fertilizer_with_max_content(surplus_nutrient)

        return (deficient_nutrient,fert1,get_favourable_crops(fert1),surplus_nutrient,fert2,get_favourable_crops(fert2))

'''	
def crop_rotation(farmer_id):

	load_data()		
	#farmer_id = 1111
    
        #print(check_farmer_exists(farmer_id))
	#crop_name,area,K,N,P,S = "ragi",50,20,10,20,50
	
	ci_df = get_current_crop_info(farmer_id)
	#print(ci_df)
	
	cn_df = get_current_nutrients_info(farmer_id)
	#print(cn_df)
	
	
	#insert_crop_info(farmer_id,crop_name,area,K,N,P,S)	
	#ci_df = get_current_crop_info(farmer_id)
	
	#print(ci_df)
	
	#cn_df = get_current_nutrients_info(farmer_id)
	#print(cn_df)
	
	surplus_nutrient,surplus_value = get_surplus_nutrient(cn_df)
	deficient_nutrient,deficient_value = get_required_nutrient(cn_df)

	fert1 = get_fertilizer_with_max_content(deficient_nutrient)
	
	fert2 = get_fertilizer_with_max_content(surplus_nutrient)
		
	return (deficient_nutrient,fert1,get_favourable_crops(fert1),surplus_nutrient,fert2,get_favourable_crops(fert2))

'''
def main():
	print(crop_rotation(1111))
	

if __name__== "__main__":
	main()
