from get_data_yield_prediction import get_yield, get_rainfall, get_temperature,predict
from trend_analysis_arima_rainfall_and_temp import rainfall_predictor,temperature_predictor
from crop_rotation import crop_rotation,insert_crop_info,check_farmer_exists
from scipy import stats
from bottle import Bottle, run, request
from bottle import template
from statsmodels.formula.api import ols
from scipy.optimize import curve_fit
import scipy
import pandas
app = Bottle()

@app.route('/yield', method="POST")
def yield_predictor():
	data=request.json
	Z=get_yield(data["state"],data["district"],data["crop"],data["season"])
	temp=len(Z)
	Z=[i[1] for i in Z]
	Yt=get_rainfall(data["state"],data["district"],data["season"])
	Y=Yt[-temp:]	
	Y=[i[1] for i in Y]
	Xt=get_temperature(data["state"],data["district"],data["season"])
	X=Xt[-temp:]	
	X=[i[1] for i in X]
	dat = pandas.DataFrame({'x': X, 'y': Y, 'z': Z})
	model = ols("z ~ x + y", dat).fit()
	print(model.summary())
	arr=(model._results.params)
	ret=(arr[0]+(arr[1]*predict(Xt,int(data["year"])))+(arr[2]*predict(Yt,int(data["year"]))))
	return str(ret*float(data['area']))

@app.route('/weather', method="POST")
def weather_predictor():
    data=request.json
    if(data["weather_type"] == "rainfall"):
        x=rainfall_predictor(data["district"],data["state"],int(data["year"]),data["season"])
        print("--")
        print(x)
        return str(x)
    elif(data["weather_type"] == "temperature"):
        return str(temperature_predictor(data["district"],data["state"],int(data["year"]),data["season"]))
    else:
        raise "Wrong selection found!"

@app.route('/crop_rotation', method="POST")
def get_crop_rotation():
	data=request.json;
	print(data["farmer_id"])
	ret=crop_rotation(data["farmer_id"])
	result={
				"insufficient":
					{
						"nutrient": ret[0],
						"fertilizer":ret[1],
						"crop":ret[2][0]
					},
				"sufficient":
				{
					"nutrient": ret[3],
					"fertilizer":ret[4],
					"crop":ret[5][0]
				}
		   }
	print(result)
	return result

@app.route('/check_farmer',method="POST")
def check_farmer():
	data=request.json;
	res=check_farmer_exists(data['farmer_id']);#print(res)
        if res=="True":
            return {"status":200}
        else:
            return {"status":500}


	
@app.route('/insert_crop_info',method="POST")
def do_insert_crop_info():
	data=request.json;
	insert_crop_info(data['farmer_id'],data['crop_name'],data['area'],data['K'],data['N'],data['P'],data['S'])
	
	
#create("Karnataka","chitradurga","Maize","Rabi")
run(app, host='0.0.0.0', port=8080)
