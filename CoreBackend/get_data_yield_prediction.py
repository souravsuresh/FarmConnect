import csv
import os
from scipy import stats

#this is dictionary for holding yield data
#this is made global so that the data is loaded once in execution

#3 seasons and their correspnding months
seasons={"Rabi":["Oct","Nov","Dec","Jan","Feb","Mar"],"Kharif":["Jul","Aug","Sep","Oct"],"Whole Year":["Oct","Nov","Dec","Jan","Feb","Mar","Jun","Jul","Aug","Sep","May","Apr"]}

#prediction using linear regression
def predict(params,year):
	slope, intercept, r_value, p_value, std_err = stats.linregress(params)
	return (slope*year+intercept)

#this predicts the weather type for the years mentioned in the list
def get_params_wrapper_predict(params):
	slope, intercept, r_value, p_value, std_err = stats.linregress(params)
	for i in ["2002","2003","2004","2005","2006","2007","2008","2009"]:
		params.append([int(i),(slope*int(i)+intercept)])
	return params

#wrapper function which populates the list params by reading a specific CSV file
def get_params_wrapper(param_type,state,district,season):	#param_type=Rainfall/temparature
	params=[]
	with open("CSV/"+param_type+"/"+state+"/"+district+".csv") as csvfile:
		reader=csv.DictReader(csvfile)
		s=0;
		for row in reader:
			if season=="Rabi":
				for i in seasons["Rabi"][3:]:
					s+=float(row[i])
				params.append([int(row["Year"])-1,s/6]);
				s=0
				
				for i in seasons["Rabi"][:3]:
					s+=float(row[i])
	params=get_params_wrapper_predict(params)			
	return params;

#wrapper function populates the yield in the below format
'''
	format of dict:
	State:
             District:
		   Crop:
		      Season:
			   [Year, Area, Production ]		
'''
d={}
#populates dictonary 'd' only once
def get_yield_wrapper():
	if d=={}:
		with open('yield.csv') as csvfile:
			reader = csv.DictReader(csvfile)
			for row in reader:
				if row["State"].strip() not in d:
					d[row["State"].strip()]={}
				if row["District"].strip() not in d[row["State"].strip()]:
					d[row["State"].strip()][row["District"].strip()	]={}
				if row["Crop"].strip() not in d[row["State"].strip()][row["District"].strip()]:	
					d[row["State"].strip()][row["District"].strip()][row["Crop"].strip()]={}
				if row["Season"].strip() not in d[row["State"].strip()][row["District"].strip()][row["Crop"].strip()]:
					d[row["State"].strip()][row["District"].strip()][row["Crop"].strip()][row["Season"].strip()]=[]
				d[row["State"].strip()][row["District"].strip()][row["Crop"].strip()][row["Season"].strip()].append([ row["Year"].strip(), (row["Area"].strip()), (row["Production"].strip()) ])
			
	
#this is the module function which client can access for yield
def get_yield(state,district,crop,season):
	get_yield_wrapper();		# this ensures that dictionary 'd' is populated
	p_a=[]
	for i in d[state.upper()][district.upper()][crop][season]:
		p_a.append([i[0],(int(i[2])/int(i[1]))])
	return p_a

#this is the module function which client can access
def get_rainfall(state,district,season):
	return get_params_wrapper("Rainfall",state,district,season)

#this is the module function which client can access
def get_temperature(state,district,season):
	return get_params_wrapper("Temperature",state,district,season)


#-----------------------------------------------------------------------
#to check

#print(get_yield("KARNATAKA","CHITRADURGA","Maize","Rabi"))
#print(get_temperature("Karnataka","chitradurga","Rabi"))

#-----------------------------------------------------------------------
	




