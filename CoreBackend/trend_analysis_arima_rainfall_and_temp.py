
import pandas as pd
from pandas import Series
from statsmodels.tsa.arima_model import ARIMA
import numpy as np
import glob
import datetime
from collections import defaultdict
#import matplotlib.pylab as plt
#from matplotlib.pylab import rcParams

#rcParams['figure.figsize'] = 15, 6

def getDirNames(dir_name):
    sub_dirs= sorted(glob.glob(dir_name +'/*'))
    return sub_dirs

def getStateFilesAsDict(all_dirs,format_of_file):
    all_files = [sorted(glob.glob(x+"/"+'*.'+format_of_file)) for x in all_dirs]
    #print(rainfall_files[0][0].split('\\'))
    state_files = defaultdict(lambda:[])
    for x in all_files:
        for y in x:
            res  = y.split('/')
            state_files[res[1]].append(y)
    return state_files

def getAverageAndAppend(city_file_path,name_of_new_column):
    data = pd.read_csv(city_file_path)
    #fill_zero = [0] * len(data['Year'])
    if name_of_new_column not in data.columns:
        avg = list()
        for i in (range(len(data['Year']))):
            sum_n = 0
            for j in range(1,13):
                sum_n += data.loc[i][j]
            avg.append(sum_n/12)
        data[name_of_new_column] = pd.Series(avg, index=data.index)
        #print(data.head(10))
        data.to_csv(city_file_path)
    #plt.plot(data[name_of_new_column])
    return pd.DataFrame(list(map(list, zip(data['Year'],data[name_of_new_column]))),columns = ['Year',name_of_new_column])

# create a differenced series
def difference(dataset, interval=1):
    diff = list()
    for i in range(interval, len(dataset)):
        value = dataset[i] - dataset[i - interval]
        diff.append(value)
    return np.array(diff)

# invert differenced value
def inverse_difference(history, yhat, interval=1):
    return yhat + history[-interval]

def predict_for_year(series,predicted_year):
    X = series.values
    differenced = difference(X)#, days_in_year)
    year = int(list(series.index)[-1])
    year_copy = year
    #print(year)
    diff_year = predicted_year - year
    # fit model
    model = ARIMA(differenced, order=(7,0,1))
    model_fit = model.fit(disp=0)
    # multi-step out-of-sample forecast
    forecast = model_fit.forecast(steps=diff_year)[0]
    # invert the differenced forecast to something usable
    history = [x for x in X]
    for yhat in forecast:
        inverted = inverse_difference(history, yhat)
        #print('Year %d: %f' % (year, inverted))
        history.append(inverted)
        year += 1
        
    #print(history[-diff_year-1:])
    #print(year_copy)
    #print(list(range(year_copy,predicted_year+1)))

    to_plot_series = pd.Series(data = history[-diff_year-1:],index =list(range(year_copy,predicted_year+1)))
    #print(series_to_plot)
    
    return (history[-1],to_plot_series)
'''
def plot_predicted(series_to_plot):
    #plt.figure()
    series_to_plot.plot()
'''
#march to may = summer
#june to oct = monsoon
#nov to feb = winter
def getAverageBySeasonsAndAppend(city_file_path,summer_name,rainy_name,winter_name):
    data = pd.read_csv(city_file_path)
    #fill_zero = [0] * len(data['Year'])
    if summer_name not in data.columns:
        avg = list()
        for i in (range(len(data['Year']))):
            sum_n = data.loc[i]['Mar']+data.loc[i]['Apr']+data.loc[i]['May']
            avg.append(sum_n/3)
        data[summer_name] = pd.Series(avg, index=data.index)
        #print(data.head(10))
        data.to_csv(city_file_path,index=False)
    #plt.plot(data[name_of_new_column])
    
    if rainy_name not in data.columns:
        avg = list()
        for i in (range(len(data['Year']))):
            sum_n = data.loc[i]['Jun']+data.loc[i]['Jul']+data.loc[i]['Sep']+data.loc[i]['Oct']
            avg.append(sum_n/4)
        data[rainy_name] = pd.Series(avg, index=data.index)
        #print(data.head(10))
        
        data.to_csv(city_file_path,index=False)
        #print(data.head(5))
        
    if winter_name not in data.columns:
        avg = list()
        for i in (range(len(data['Year']))):
            sum_n = data.loc[i]['Nov']+data.loc[i]['Dec']+data.loc[i]['Jan']+data.loc[i]['Feb']
            avg.append(sum_n/4)
        data[winter_name] = pd.Series(avg, index=data.index)
        #print(data.head(10))
        
        data.to_csv(city_file_path,index=False)
        #print(data.head(5))
    
    return pd.DataFrame(list(map(list, zip(data['Year'],data[summer_name],data[rainy_name],data[winter_name]))),columns = ['Year',summer_name,rainy_name,winter_name])

def getDataForMonth(city_file_path,month):
    data = pd.read_csv(city_file_path)
    return pd.DataFrame(list(map(list, zip(data['Year'],data[month]))),columns = ['Year',month])

def calculate_avg_per_year_and_plot(every_city_file_path,new_attribute,year):
    df = getAverageAndAppend(every_city_file_path,new_attribute)
    series = pd.Series(data = df[new_attribute].values,index =df['Year'])
    expected,to_plot = predict_for_year(series,year)
    #plot_predicted(to_plot)
    return expected

def calculate_avg_per_season_and_plot_(every_city_file_path,wanted_,df,year):
    series = pd.Series(data = df[wanted_].values,index =df['Year'] )
    expected,to_plot = predict_for_year(series,year)
    #plot_predicted(to_plot)
    return expected

def calculate_avg_per_season_and_plot(every_city_file_path,wanted_,year):
    df = getAverageBySeasonsAndAppend(every_city_file_path,"avg_summer","avg_rainy","avg_winter")
    return calculate_avg_per_season_and_plot_(every_city_file_path,wanted_,df,year)

def calculate_per_month_and_plot(every_city_file_path,month,year):
    df = getDataForMonth(every_city_file_path,month)
    series = pd.Series(data = df[month].values,index =df['Year'] )
    expected,to_plot = predict_for_year(series,year)
    #plot_predicted(to_plot)
    return expected

def rainfall_predictor(every_city,every_state,year,month_season):
    # load dataset
    rain_dir_name = 'CSV/Rainfall'
    series = ""
    df = ""
    #every_city = "adilabad"
    #every_state = "Andhra Pradesh"
    
    
    state_names = getDirNames(rain_dir_name);
    all_files = getStateFilesAsDict(state_names,'csv');
    #pprint(all_files)
    
    every_city_file_path = rain_dir_name+"/"+every_state+"/"+every_city+".csv"
    #print(every_city_file_path)
    
    
    if(len(month_season) ==0):
        expected = calculate_avg_per_year_and_plot(every_city_file_path,"avg_rainfall",year)
        print('avg rainfall for Year %d: %f cm for region : %s , state : %s' % (year, expected,every_city,every_state)) 
        return expected
    elif( month_season[0] == "month"):
        mydict = {'January':"Jan","Febraury":"Feb","March":'Mar',"April":"Apr","May":"May","June":"June","July":"July","August":"Aug","September":"Sept","October":"Oct","November":"Nov","December":"Dec"}
        month = mydict[month_season[1]]
        expected = calculate_per_month_and_plot(every_city_file_path,month,year)
        print('avg rainfall in the month of %s for Year %d: %f cm for region : %s , state : %s' % (month,year, expected,every_city,every_state))
        return expected
    elif(month_season[0] == "season"):
        season_name = "avg_"+month_season[1]
        expected = calculate_avg_per_season_and_plot(every_city_file_path,season_name,year)
        print('avg rainfall in %s season for Year %d: %f cm for region : %s , state : %s' % (month_season[1],year, expected,every_city,every_state))
        return expected
    else:
        raise "Invalid Argument Exception"

def temperature_predictor(every_city,every_state,year,month_season):
    # load dataset
    rain_dir_name = 'CSV/Temperature'
    series = ""
    df = ""
    #every_city = "adilabad"
    #every_state = "Andhra Pradesh"
    
    
    state_names = getDirNames(rain_dir_name);
    all_files = getStateFilesAsDict(state_names,'csv');
    #pprint(all_files)
    
    every_city_file_path = rain_dir_name+"/"+every_state+"/"+every_city+".csv"
    #print(every_city_file_path)
    
    
    if(len(month_season) ==0):
        expected = calculate_avg_per_year_and_plot(every_city_file_path,"avg_rainfall",year)
        print('avg temperature for Year %d: %f C for region : %s , state : %s' % (year, expected,every_city,every_state))
        return expected
    elif( month_season[0] == "month"):
        mydict = {'January':"Jan","Febraury":"Feb","March":'Mar',"April":"Apr","May":"May","June":"June","July":"July","August":"Aug","September":"Sept","October":"Oct","November":"Nov","December":"Dec"}
        month = mydict[month_season[1]]
        expected = calculate_per_month_and_plot(every_city_file_path,month,year)
        print('avg temperature in the month of %s for Year %d: %f C for region : %s , state : %s' % (month,year, expected,every_city,every_state))
        return expected
    elif(month_season[0] == "season"):
        season_name = "avg_"+month_season[1]
        expected = calculate_avg_per_season_and_plot(every_city_file_path,season_name,year)
        print('avg temperature in %s season for Year %d: %f C for region : %s , state : %s' % (month_season[1],year, expected,every_city,every_state))
        return expected
    
    else:
        raise "Invalid Argument Exception"

'''
now = datetime.datetime.now()
main('adilabad','Andhra Pradesh',"rainfall",now.year,["season","winter"])
main('adilabad','Andhra Pradesh',"rainfall",now.year,["month","March"])
main('adilabad','Andhra Pradesh',"rainfall",now.year,[])
main('adilabad','Andhra Pradesh',"temperature",now.year,["season","winter"])
main('adilabad','Andhra Pradesh',"temperature",now.year,["month","March"])
main('adilabad','Andhra Pradesh',"temperature",now.year,[])
'''
